---
title: Accepted Talks
---

{% assign talks = site.talks | where: 'type', 'talk' %}
{% for talk in talks %}
  <ol class="talks">
    <li>
      <h3><a href="{{ talk.url }}">{{ talk.title }}</a></h3>
      <h4>{{ talk.speakers | join: ', ' }}</h4>

      {{ talk.abstract | markdownify }}
    </li>
  </ol>
{% endfor %}
