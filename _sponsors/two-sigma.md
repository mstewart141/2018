---
name: Two Sigma
tier: silver
site_url: https://www.twosigma.com
logo: two-sigma.png
---
Two Sigma is a different kind of investment manager. We view the world through a technology lens. We
hire scientists, software engineers and academics who tackle the world’s most challenging financial
problems. We look for solutions grounded in data science and research, and powered by technology.
And we are finding ways for our platform to reshape new industries, from insurance and securities to
private investments and venture-backed startups. Come seek with us.
