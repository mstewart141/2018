---
duration: 45
presentation_url:
room: PennTop South
slot: 2018-10-05 09:15:00-04:00
speakers:
- Karen M. Sandler
title: Software Freedom And Ethical Technology
type: talk
video_url: https://youtu.be/iIDZNz22uxI
---

As a lawyer and free software enthusiast with a life-critical medical
device implanted in their body, Karen Sandler has come to understand how the
critical choices we are making about software will have huge societal
impact. In the age of corporate surveillance and walled-garden platforms,
where every week there is a new known software vulnerability, software
freedom is more important than ever.

Karen will touch on potential avenues for accountability, transparency, and
access to remedies as we hurtle towards an Internet of Things built on
proprietary source code that prevents us from knowing exactly how these
vital devices work, what data they are collecting and to what ends, what
their vulnerabilities might be, and the extent to which their closed,
proprietary nature keeps us from developing societal mechanisms and review
processes to keep us safe.
