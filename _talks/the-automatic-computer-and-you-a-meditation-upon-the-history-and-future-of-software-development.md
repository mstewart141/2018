---
duration: 45
presentation_url:
room: PennTop South
slot: 2018-10-06 09:15:00-04:00
speakers:
- Glyph
title: 'The Automatic Computer and You: A Meditation Upon The History And Future Of
  Software Development'
type: talk
---

As members of the Python community, we all believe that computers have the
_potential_ to do great things.

But are the things we're doing with computers, overall, _actually_ great?
What is the _purpose_ of computers?  Why do we build software for them?  How
can we do it better?  And: should we be making anything other than video
games?

In this talk, Glyph will answer those questions, and more; some of his
proposed answers might surprise you.  He will present extensive digressions
on the history of the craft and industry of software development, the impact
that recent events have had on the course of that history, and he will
contemplate new directions which might lead toward a brighter tomorrow.
Along the way, he will consider what lessons we might learn from the
progression of other, similar fields, such as medieval church architecture
and nuclear power generation.
