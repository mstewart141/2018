---
abstract: Coding by voice can provide benefits from reducing RSI and pain to great
  productivity boosts. Unfortunately, setting up a voice code environment is not as
  easy at it should be. Learn about setting up a voice environment workflow using
  Python, Dragonfly, and your favorite editor!
duration: 30
level: All
presentation_url:
room: PennTop North
slot: 2018-10-06 13:30:00-04:00
speakers:
- Boudewijn Aasman
title: Coding by Voice with Dragonfly
type: talk
video_url: https://youtu.be/P5DCDiCv4TE
---

In 2013, Travis Rudd showed the world that coding by voice is a reality, saving himself and other developers from RSI and other related injuries. Unfortunately, setting up a voice code environment is more than trivial. It requires several components layered on top of each other, and the building of your own voice grammar. In addition, since a standard for doing this has not yet emerged, the vast majority of people end up reinventing the wheel. With so many new voice powered applications coming out, we are on the cusp of a game-changing way to code.

 In this talk, you'll learn about:

    1. What voice recognition software to choose, and the limited options available
    2. Natlink - the piece of software that makes all of this possible
    3. Dragonfly - A python package that allows for building 'grammars'
    4. Best practices for building these grammars, and other useful tips
    5. What the future of voice coding looks like

I will also do a small demo by writing a piece of code entirely by voice.
