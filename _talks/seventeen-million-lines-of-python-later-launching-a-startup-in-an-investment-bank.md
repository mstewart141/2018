---
abstract: In the wake of the 2008 financial crisis, Bank of America Merrill Lynch
  embarked on an ambitious plan to realign its sales and trading systems around the
  Python-based Quartz platform.  More than half way through this journey, we have
  millions of lines of Python under our belt and major business deliverables behind
  us.  This talk will cover the unique benefits and challenges of using Python at
  investment bank-scale, and how to make a startup initiative stick in the face of
  unrelenting impediments.
duration: 30
level: All
presentation_url:
room: PennTop North
slot: 2018-10-06 10:15:00-04:00
speakers:
- Garrett Walker
title: 'Seventeen Million Lines of Python Later: Launching a Startup in an Investment
  Bank'
type: talk
---
