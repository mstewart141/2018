---
abstract: "Stop and look around you. Are you next to people who look and think like
  you? \nThrough grassroots efforts by fellow software engineers, we at Kyruus, a
  healthcare startup, are rethinking the entire lifecycle of diversity and inclusion:
  from the hiring process, to interviews, to daily interactions."
duration: 25
level: All
presentation_url:https://drive.google.com/file/d/1_kmwEkPdoptC7kLu5pt1keyRYKSBaiAC/view?usp=sharing
room: Madison
slot: 2018-10-05 16:55:00-04:00
speakers:
- Michelle Chen
title: Diversity and Inclusion for all!
type: talk
video_url: https://youtu.be/lXdfYzNmikI
---

The average tech company has a rough female representation of 25%. The question that is always asked is: Why are there not more women and minorities in the tech industry? 

At Kyruus, we have been thinking about how to tackle this issue and address the lack of diversity by: 

* Improving our hiring pipeline
* Addressing challenges during the interview process
* Holding focus groups and sharing with the entire company 
* Discussing how to think about career growth

The majority of these efforts started with us, the software engineers. We continue our efforts because there is so much more that we can do to improve diversity in the tech industry. This talk will address what we have done, but also some of the challenges that we faced.

Come to this talk to learn more about what your company can be doing to make your workplace more inclusive. Everyone can help!
