---
abstract: Have you ever written an application only to find yourself rewriting it
  later so it can scale? Many of us have. In this talk, you'll learn scale independent
  computing; what it is and how it can help you more easily scale your applications.
duration: 30
level: All
presentation_url:
room: PennTop North
slot: 2018-10-06 15:10:00-04:00
speakers:
- Sean T Allen
title: 'Scale Independent Python: How to scale your Python application without any
  code changes'
type: talk
video_url: https://youtu.be/NZJLTInqifg
---

Scaling applications is hard. Our typical approaches all come with tradeoffs that we wish we didn't have to make. There is a better way, scale-independent computing. Based on ideas from Pat Helland's paper "Beyond Distributed Transactions," scale-independent computing allows application developers to write their code without regard to scale and then run it on a scale-aware platform. The idea is relatively simple, the code you write should be able to run across any number of Python processes without you having to change any of your code. During this talk, you'll learn the core ideas of scale-independence and see an example scale-independent application in action.
