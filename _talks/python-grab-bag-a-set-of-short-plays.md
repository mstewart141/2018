---
abstract: "A frenetic combination of educational and entertaining segments, as chosen
  by the audience! They\u2019re all different - plays, monologues, physical demos,
  jokes, or tiny traditional conference talks - and the audience chooses which they
  want next from a menu of presentation titles."
duration: 45
level: All
presentation_url:
room: PennTop South
slot: 2018-10-06 11:15:00-04:00
speakers:
- Jason Owen
- Sumana Harihareswara
title: 'Python Grab Bag: A Set of Short Plays'
type: talk
---

A frenetic combination of educational and entertaining segments, as chosen
by the audience! In between segments, audience members will shout out
numbers from a menu, and we’ll perform the selected segment: it may be a
short monologue, it may be a play, it may be a physical demo, or it may be a
tiny traditional conference talk.

Audience members should walk away with some additional understanding of the
history of Python, knowledge of some tools and libraries available in the
Python ecosystem, and some Python-related amusement.

This talk is inspired by [the Neo-Futurists’ Infinite
Wrench](http://www.nyneofuturists.org/), a creative and energetic piece of
theater.

The audience can select from these plays:

1. The Unvarnished Truth
2. <tt>from import import import</tt>
3. WHAT'S the DEAL with CLIENTS?
4. A Play Entirely Full of Monty Python References
5. A Proposal for Explaining PEPs
6. GNU Mailman: A Pythonic Playlist
7. Soup, Scrape, Sweep
8. Generators: Taste the Freshness
9. This Is How We Do It
10. Cookie For Your Thoughts
11. If Shakespeare Wrote Incident Reports
12. Code Review: Fast Forward and Back
13. When The Old Was New
14. Things We Don't Say At The Daily Standup Meeting
15. The Relief of Reuse (The Colorful <tt>argparse</tt> Play)
16. Be A Better Bureaucrat (The Intellectual <tt>argparse</tt> Play)
17. Speaking Python
18. The End (Of 2.7) Is Near (feat. Jason as Guido van Rossum)
