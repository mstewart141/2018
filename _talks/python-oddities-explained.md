---
abstract: Sometimes you'll find what might seem like a bug in Python but sometimes
  these bugs reveal themselves to be misunderstood features. During this talk we'll
  look at a number of Python's unique features and quirks and attempt to re-shape
  our mental models of how Python works.
duration: 45
level: Intermediate
presentation_url: http://treyhunner.com/python-oddities/
room: PennTop South
slot: 2018-10-05 11:15:00-04:00
speakers:
- Trey Hunner
title: Python Oddities Explained
type: talk
video_url: https://youtu.be/NZyNOPbMSwQ
---

A number of Python features often seem counter-intuitive at first glance, especially when moving from another programming language to Python. Often what at first seems like a bug, will later reveal itself to be a misunderstood feature.

During this talk we'll look at a number of Python's unique features and quirks and attempt to re-shape our mental models of Python to better match reality. By the end of this talk you'll have a deeper understanding of Python's rules behind objects, scope, and variables.

Warning: this talk will include many Python head scratchers so show up prepared to think on your feet!