---
name: Christian Heimes
talks:
- 'Everyday security issues and how to avoid them'
---

Christian is a long time Python developer from Hamburg/Germany. In the past
he has contributed to several Open Source projects such as the CPython
interpreter. In the past years he has helped to keep Python secure, for
example as member of the Python security response team, secure hashing (PEP
456) and improvements of Python’s TLS/SSL module. Nowadays he is employed by
Red Hat and works OpenShift container security and FreeIPA identity
management.
