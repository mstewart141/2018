---
name: Kelley Robinson
talks:
- Practical Cryptography
---

Kelley has worked in a variety of engineering roles, ranging from trading live cattle derivatives to building data infrastructure with Python and Spark. As a developer evangelist at Twilio she spends a lot of time thinking about how to make technical concepts accessible to new audiences. In her spare time, Kelley greatly enjoys cooking and reorganizing her tiny kitchen to accommodate completely necessary small appliance purchases.