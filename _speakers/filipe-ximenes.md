---
name: Filipe Ximenes
talks:
- Pluggable Libs Through Design Patterns
---

I'm a Python/Django developer from Brazil. I work in a Django shop I started with a couple friends and I'm passionate about developing beautiful code. I'm interested in all kinds of programming languages, their communities and open source. I believe simple is better than complex, and that this should be a mantra for most things in life. I value human relationships and the way we, as a society, interact with the place we live in. For that, I recognize the use of bicycles for transportation and the use of public space as a priority to build healthy cities.