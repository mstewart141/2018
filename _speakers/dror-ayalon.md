---
name: Dror Ayalon
talks:
- Creative Music Applications in Python
---

[Dror Ayalon](https://www.drorayalon.com) is a software engineer, product manager, and interaction designer. He researches and develops innovative music creation tools, using music information retrieval (MIR) techniques, digital signal processing (DSP), and machine learning algorithms, that will allow musicians to compose music in a variety of new ways and formats.  
  
As a Product Manager, Dror worked for almost 5 years in the start-up industry, most notably for [Viber](https://www.viber.com/), from its early days until its [$900M acquisition](https://techcrunch.com/2014/02/13/japanese-internet-giant-rakuten-acquires-viber-for-900m/).  
  
Dror received his bachelor's degree from The Interdisciplinary Center, Herzliya (IDC, Israel) in Communications, Interactive Media and Human-Computer Interactions, not before he completed a full academic year at the IDC [Media Innovation Lab](http://milab.idc.ac.il/). Recently, Dror received his master's degree from [New York University's Interactive Telecommunication Program (ITP)](https://tisch.nyu.edu/itp). Post his graduation, Dror joined 	the [Google Creative Lab](https://experiments.withgoogle.com/) in NYC.  
  
Dror developed and published a variety of Python-centric applications, open-source projects. For more information and projects:  
  
- [Dror's personal portfolio](https://www.drorayalon.com)  
- [Dror's GitHub account](https://github.com/dodiku)